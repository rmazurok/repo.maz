DROP TABLE managers CASCADE;
DROP TABLE clients CASCADE;
DROP TABLE resorts CASCADE;
DROP TABLE tracks CASCADE;
DROP TABLE skipasses CASCADE;
DROP TABLE hotels CASCADE;
DROP TABLE rooms CASCADE;
DROP TABLE rent_office CASCADE;
DROP TABLE sells CASCADE;
DROP TABLE booking CASCADE;
DROP TABLE rents CASCADE;

CREATE TABLE managers
(
  manager_id SERIAL PRIMARY KEY,
  manager_name name_surname NOT NULL,
  manager_surname name_surname NOT NULL,
  manager_login text NOT NULL
  CHECK (manager_login ~* '^[A-Za-z0-9_.-]+$')
);

CREATE TABLE clients
(
  client_id SERIAL PRIMARY KEY,
  client_name name_surname NOT NULL,
  client_surname name_surname NOT NULL,
  client_tel_number tel_number NOT NULL,
  client_credit_card text NOT NULL,
  manager_id integer NOT NULL,
  FOREIGN KEY (manager_id) REFERENCES managers (manager_id) ON DELETE CASCADE,
  CHECK (client_credit_card ~* '[0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9]')
);

CREATE TABLE resorts
(
  resort_id SERIAL PRIMARY KEY, 
  resort_name name_surname UNIQUE, 
  resort_site text UNIQUE, 
  resort_tracks_number integer NOT NULL,
  resort_tel_number tel_number NOT NULL,
  CHECK (resort_site ~* '^[A-Za-z_./-]+$')
);

CREATE TABLE tracks
(
  track_id SERIAL PRIMARY KEY,
  track_difficulty difficulty NOT NULL,
  track_length integer NOT NULL,
  resort_id integer NOT NULL,
  FOREIGN KEY (resort_id) REFERENCES resorts (resort_id) ON DELETE CASCADE,
  CHECK (track_length > 0)	  
);

CREATE TABLE skipasses
(
  skipass_id SERIAL PRIMARY KEY,
  skipass_type skipasstype NOT NULL,
  skipass_days SMALLINT NOT NULL,
  skipass_price numeric(10,2) NOT NULL,
  resort_id integer NOT NULL,
  FOREIGN KEY (resort_id) REFERENCES resorts (resort_id) ON DELETE CASCADE,
  CHECK (skipass_days > 0),
  CHECK (skipass_price > 0)
);

CREATE TABLE hotels
(
  hotel_id SERIAL PRIMARY KEY,
  hotel_name name_surname UNIQUE,
  hotel_tel_number tel_number NOT NULL,
  hotel_manager name_surname NOT NULL,
  resort_id integer NOT NULL,
  FOREIGN KEY (resort_id) REFERENCES resorts (resort_id) ON DELETE CASCADE
);

CREATE TABLE rooms
(
  room_id SERIAL PRIMARY KEY,
  room_class roomclass NOT NULL,
  room_people_number smallint NOT NULL,
  room_price numeric(10,2) NOT NULL,
  room_status roomstatus NOT NULL,
  hotel_id integer NOT NULL,
  FOREIGN KEY (hotel_id) REFERENCES hotels (hotel_id) ON DELETE CASCADE
);

CREATE TABLE rent_office
(
  stuff_id SERIAL PRIMARY KEY,
  stuff_type stufftype NOT NULL,
  stuff_rent_price numeric(10,2) NOT NULL
);

CREATE TABLE sells
(
  sell_id SERIAL PRIMARY KEY,
  skipass_id integer NOT NULL,
  sell_number integer NOT NULL,
  sell_price numeric(10,2) NOT NULL,
  client_id integer NOT NULL,
  FOREIGN KEY (client_id) REFERENCES clients (client_id) ON DELETE CASCADE,
  FOREIGN KEY (skipass_id) REFERENCES skipasses (skipass_id) ON DELETE CASCADE
);

CREATE TABLE booking
(
  booking_id SERIAL PRIMARY KEY,
  booking_begin_date date NOT NULL,
  booking_end_date date NOT NULL,
  room_id integer NOT NULL,
  booking_price numeric(10,2) NOT NULL,
  client_id integer NOT NULL,
  FOREIGN KEY (client_id) REFERENCES clients (client_id) ON DELETE CASCADE,
  FOREIGN KEY (room_id) REFERENCES rooms (room_id) ON DELETE CASCADE,
  CHECK (booking_begin_date >= 'now'::text::date AND booking_begin_date < booking_end_date),
  CHECK (booking_end_date > 'now'::text::date AND booking_end_date > booking_begin_date)
);

CREATE TABLE rents
(
  rent_id SERIAL PRIMARY KEY,
  snowboard_number smallint NOT NULL,
  ski_number smallint NOT NULL,
  helmet_number smallint NOT NULL,
  glasses_number smallint NOT NULL,
  armor_number smallint NOT NULL,
  clothes_number smallint NOT NULL,
  rent_days integer NOT NULL,
  rent_bail bail NOT NULL,
  rent_price numeric(10,2) NOT NULL,
  rent_fine numeric(10,2) NOT NULL,
  rent_status rentstatus NOT NULL,
  client_id integer NOT NULL,
  FOREIGN KEY (client_id) REFERENCES clients (client_id) ON DELETE CASCADE,
  CHECK (snowboard_number >=0 ),
  CHECK (ski_number >=0 ),
  CHECK (helmet_number >=0 ),
  CHECK (glasses_number >=0 ),
  CHECK (armor_number >=0 ),
  CHECK (clothes_number >=0 )
);



