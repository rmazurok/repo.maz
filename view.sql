CREATE OR REPLACE VIEW all_operations_normal(operation, operation_id, price, client_id) AS 
	(SELECT 'sell' AS operation, sells.sell_id AS operation_id, sells.sell_price, sells.client_id FROM sells WHERE sells.sell_price < 1000 UNION
	 SELECT 'booking' AS operation, booking.booking_id AS operation_id, booking.booking_price, booking.client_id FROM booking WHERE booking.booking_price < 1000 UNION
	 SELECT 'rent' AS operation, rents.rent_id AS operation_id, rents.rent_price, rents.client_id FROM rents WHERE rents.rent_price < 1000) ORDER BY operation, operation_id ASC;

CREATE OR REPLACE VIEW all_operations_vip(operation, operation_id, price, client_id) AS 
	(SELECT 'sell' AS operation, sells.sell_id AS operation_id, sells.sell_price, sells.client_id FROM sells WHERE sells.sell_price >= 1000 UNION
	 SELECT 'booking' AS operation, booking.booking_id AS operation_id, booking.booking_price, booking.client_id FROM booking WHERE booking.booking_price >= 1000 UNION
	 SELECT 'rent' AS operation, rents.rent_id AS operation_id, rents.rent_price, rents.client_id FROM rents WHERE rents.rent_price >= 1000) ORDER BY operation, operation_id ASC;
	
--Розмежування доступу
CREATE ROLE manager_normal;
CREATE ROLE manager_vip;
GRANT SELECT (operation, operation_id, price) ON all_operations_normal TO manager_normal;
GRANT SELECT (operation, operation_id, price) ON all_operations_vip TO manager_vip;
GRANT SELECT ON all_operations_normal, all_operations_vip TO manager, administrator;
REVOKE ALL PRIVILEGES ON sells, booking, rents FROM manager_normal, manager_vip;

--INSERT RULE
CREATE OR REPLACE RULE all_operations_INSERT AS ON INSERT TO all_operations_normal DO INSTEAD
(
    INSERT INTO  sells(sell_id, sell_price, client_id) VALUES (NEW.operation_id, NEW.price, NEW.client_id);
    INSERT INTO  booking(booking_id, booking_price, client_id) VALUES (NEW.operation_id, NEW.price, NEW.client_id);
	INSERT INTO  rents(rent_id, rent_price, client_id) VALUES (NEW.operation_id, NEW.price, NEW.client_id);
);

CREATE OR REPLACE RULE all_operations_INSERT AS ON INSERT TO all_operations_vip DO INSTEAD
(
    INSERT INTO  sells(sell_id, sell_price, client_id) VALUES (NEW.operation_id, NEW.price, NEW.client_id);
    INSERT INTO  booking(booking_id, booking_price, client_id) VALUES (NEW.operation_id, NEW.price, NEW.client_id);
	INSERT INTO  rents(rent_id, rent_price, client_id) VALUES (NEW.operation_id, NEW.price, NEW.client_id);
);

--UPDATE RULE
CREATE OR REPLACE RULE all_operations_UPDATE AS ON UPDATE TO all_operations_normal DO INSTEAD
(
    UPDATE sells SET sell_id = NEW.operation_id, sell_price = NEW.price, client_id = NEW.client_id;
    UPDATE booking SET booking_id = NEW.operation_id, booking_price = NEW.price, client_id = NEW.client_id;
	UPDATE rents SET rent_id = NEW.operation_id, rent_price = NEW.price, client_id = NEW.client_id;
);

CREATE OR REPLACE RULE all_operations_UPDATE AS ON UPDATE TO all_operations_vip DO INSTEAD
(
    UPDATE sells SET sell_id = NEW.operation_id, sell_price = NEW.price, client_id = NEW.client_id;
    UPDATE booking SET booking_id = NEW.operation_id, booking_price = NEW.price, client_id = NEW.client_id;
	UPDATE rents SET rent_id = NEW.operation_id, rent_price = NEW.price, client_id = NEW.client_id;
);
	 
--DELETE RULE
CREATE OR REPLACE RULE all_operations_DELETE AS ON DELETE TO all_operations_normal DO INSTEAD
(
    DELETE FROM sells WHERE sell_id = OLD.operation_id;
    DELETE FROM booking WHERE booking_id = OLD.operation_id;
	DELETE FROM rents WHERE rent_id = OLD.operation_id;
);

CREATE OR REPLACE RULE all_operations_DELETE AS ON DELETE TO all_operations_vip DO INSTEAD
(
    DELETE FROM sells WHERE sell_id = OLD.operation_id;
    DELETE FROM booking WHERE booking_id = OLD.operation_id;
	DELETE FROM rents WHERE rent_id = OLD.operation_id;
);

CREATE OR REPLACE FUNCTION all_operations_update() RETURNS TRIGGER AS $all_operations_update$
	BEGIN
	IF (TG_OP = 'INSERT') THEN
		INSERT INTO  sells(sell_id, sell_price, client_id) VALUES (NEW.operation_id, NEW.price, NEW.client_id);
		INSERT INTO  booking(booking_id, booking_price, client_id) VALUES (NEW.operation_id, NEW.price, NEW.client_id);
		INSERT INTO  rents(rent_id, rent_price, client_id) VALUES (NEW.operation_id, NEW.price, NEW.client_id);
	RETURN NEW;
	ELSIF (TG_OP = 'UPDATE') THEN
	    UPDATE sells SET sell_id = NEW.operation_id, sell_price = NEW.price, client_id = NEW.client_id;
		UPDATE booking SET booking_id = NEW.operation_id, booking_price = NEW.price, client_id = NEW.client_id;
		UPDATE rents SET rent_id = NEW.operation_id, rent_price = NEW.price, client_id = NEW.client_id;
	RETURN NEW;
	ELSIF (TG_OP = 'DELETE') THEN
		DELETE FROM sells WHERE sell_id = OLD.operation_id;
		DELETE FROM booking WHERE booking_id = OLD.operation_id;
		DELETE FROM rents WHERE rent_id = OLD.operation_id;
	RETURN OLD;
	END IF;
	RETURN NULL;
	END;
$all_operations_update$ LANGUAGE plpgsql;
CREATE TRIGGER all_operations_normal_update INSTEAD OF INSERT OR UPDATE OR DELETE ON all_operations_normal
FOR EACH ROW EXECUTE PROCEDURE all_operations_update();
CREATE TRIGGER all_operations_vip_update INSTEAD OF INSERT OR UPDATE OR DELETE ON all_operations_vip
FOR EACH ROW EXECUTE PROCEDURE all_operations_update();