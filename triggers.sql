--��������� �������� ��������
--��� ������� hotels
--DROP TRIGGER hotels_check_FK  ON hotels CASCADE;
--DROP FUNCTION hotels_check_FK() CASCADE;

CREATE OR REPLACE FUNCTION hotels_check_FK() RETURNS TRIGGER AS $hotels_check_FK$
	DECLARE
		temp RECORD;
	BEGIN
		IF NEW.resort_id IS NOT NULL THEN
			SELECT INTO temp resort_id FROM resorts WHERE resort_id = NEW.resort_id;
			IF NOT FOUND THEN RAISE EXCEPTION 'this foreign key resort_id does not exist';
			END IF;
		END IF;
		RETURN NEW;
	END;
$hotels_check_FK$ LANGUAGE plpgsql;

CREATE TRIGGER hotels_check_FK BEFORE INSERT OR UPDATE ON hotels
FOR EACH ROW EXECUTE PROCEDURE hotels_check_FK();

--���������, �� ������ ������� ������ ������ ��� �������� �������
--DROP TRIGGER managers_check  ON managers CASCADE;
--DROP FUNCTION managers_check() CASCADE;
--DROP TABLE managers CASCADE;


CREATE TABLE managers --��������� ���� ������� ��� �������� NOT NULL �� ��'�, ������� �� ����
(
	manager_id SERIAL PRIMARY KEY,
	manager_name name_surname,
	manager_surname name_surname,
	manager_login text
	CHECK (manager_login ~* '^[A-Za-z0-9_.-]+$')
);

CREATE OR REPLACE FUNCTION managers_check() RETURNS TRIGGER AS $managers_check$
	BEGIN
		IF NEW.manager_name IS NULL THEN
			RAISE EXCEPTION 'manager_name cannot be null';
		END IF;
		IF NEW.manager_surname IS NULL THEN
			RAISE EXCEPTION 'manager_surmane cannot be null';
		END IF;
		IF NEW.manager_login IS NULL THEN
			RAISE EXCEPTION 'manager_login cannot be null';
		END IF;
		RETURN NEW;
	END;
$managers_check$ LANGUAGE plpgsql;
CREATE TRIGGER managers_check BEFORE INSERT OR UPDATE ON managers
FOR EACH ROW EXECUTE PROCEDURE managers_check();

--����������� ��������� ����������� (����������) ����(-��)
--DROP TRIGGER sells_price_count ON sells CASCADE;
--DROP FUNCTION sells_price_count() CASCADE;

CREATE OR REPLACE FUNCTION sells_price_count() RETURNS TRIGGER AS $sells_price_count$
	DECLARE
		price NUMERIC(10,2);
	BEGIN
		SELECT INTO price skipass_price FROM skipasses WHERE skipasses.skipass_id = NEW.skipass_id;
		NEW.sell_price = NEW.sell_number * price;
		RETURN NEW;
	END;
$sells_price_count$ LANGUAGE plpgsql;

CREATE TRIGGER sells_price_count BEFORE INSERT OR UPDATE ON sells
FOR EACH ROW EXECUTE PROCEDURE sells_price_count();

--DROP TRIGGER booking_price_count ON sells CASCADE;
--DROP FUNCTION booking_price_count() CASCADE;

CREATE OR REPLACE FUNCTION booking_price_count() RETURNS TRIGGER AS $booking_price_count$
	DECLARE
		price NUMERIC(10,2);
	BEGIN
		SELECT INTO price room_price FROM rooms WHERE rooms.room_id = NEW.room_id;
		NEW.booking_price = (NEW.booking_end_date - NEW.booking_begin_date) * price;
		RETURN NEW;
	END;
$booking_price_count$ LANGUAGE plpgsql;

CREATE TRIGGER rents_price_count BEFORE INSERT OR UPDATE ON rents
FOR EACH ROW EXECUTE PROCEDURE rents_price_count();

--DROP TRIGGER rents_price_count ON rents CASCADE;
--DROP FUNCTION rents_price_count() CASCADE;

CREATE OR REPLACE FUNCTION rents_price_count() RETURNS TRIGGER AS $rents_price_count$
	DECLARE
		
		snowboard_price NUMERIC(10,2);
		ski_price NUMERIC(10,2);
		helmet_price NUMERIC(10,2);
		glasses_price NUMERIC(10,2);
		armor_price NUMERIC(10,2);
		clothes_price NUMERIC(10,2);
	BEGIN
		SELECT INTO snowboard_price stuff_rent_price FROM rent_office WHERE stuff_type = 'snowboard';
		SELECT INTO ski_price stuff_rent_price FROM rent_office WHERE stuff_type = 'ski';
		SELECT INTO helmet_price stuff_rent_price FROM rent_office WHERE stuff_type = 'helmet';
		SELECT INTO glasses_price stuff_rent_price FROM rent_office WHERE stuff_type = 'glasses';
		SELECT INTO armor_price stuff_rent_price FROM rent_office WHERE stuff_type = 'armor';
		SELECT INTO clothes_price stuff_rent_price FROM rent_office WHERE stuff_type = 'clothes';
		
		NEW.rent_price = 
			(NEW.snowboard_number * snowboard_price + 
			NEW.ski_number * ski_price +
			NEW.helmet_number * helmet_price +
			NEW.glasses_number * glasses_price +
			NEW.armor_number * armor_price +
			NEW.clothes_number * clothes_price) * NEW.rent_days;
		RETURN NEW;
	END;
$rents_price_count$ LANGUAGE plpgsql;

CREATE TRIGGER rents_price_count BEFORE INSERT OR UPDATE ON rents
FOR EACH ROW EXECUTE PROCEDURE rents_price_count();

--������������ ��� ��� ������ � �������
--DROP TRIGGER sells_audit ON sells CASCADE;
--DROP FUNCTION sells_audit() CASCADE;
--DROP TABLE sells_audit CASCADE;

CREATE TABLE sells_audit
(
	operation CHAR(1) NOT NULL,
	stamp TIMESTAMP NOT NULL,
	sells_id SERIAL NOT NULL, 
	skipass_id integer NOT NULL,
	sell_number integer NOT NULL,
	sell_price numeric(10,2) NOT NULL,
	client_id integer NOT NULL
);

CREATE OR REPLACE FUNCTION sells_audit() RETURNS TRIGGER AS $sells_audit$
	BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO sells_audit SELECT 'D', NOW(), OLD.*;
		RETURN OLD;
	ELSIF (TG_OP = 'UPDATE') THEN
        INSERT INTO sells_audit SELECT 'U', NOW(), NEW.*;
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN
        INSERT INTO sells_audit SELECT 'I', NOW(), NEW.*;
        RETURN NEW;
    END IF;
	RETURN NULL;
	END;
$sells_audit$ LANGUAGE plpgsql;

CREATE TRIGGER sells_audit AFTER INSERT OR UPDATE OR DELETE ON sells
FOR EACH ROW EXECUTE PROCEDURE sells_audit();