CREATE ROLE dbcreator WITH SUPERUSER CREATEROLE CREATEDB LOGIN;
CREATE ROLE administrator WITH CREATEROLE LOGIN;
CREATE ROLE manager WITH LOGIN;
CREATE ROLE client WITH NOLOGIN;

GRANT CONNECT ON DATABASE tourfirm TO administrator;
GRANT CONNECT ON DATABASE tourfirm TO manager;

GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO administrator;

GRANT SELECT, UPDATE ON resorts, tracks, skipasses, hotels, rooms, rent_office TO manager;
GRANT SELECT, INSERT, UPDATE ON clients, sells, booking, rents TO manager;

GRANT SELECT ON resorts, tracks, hotels TO client;
GRANT SELECT(skipass_type, skipass_days, skipass_price, resort_id) ON skipasses TO client;
GRANT SELECT(room_class, room_people_number, room_price, room_status, hotel_id) ON rooms TO client;
GRANT SELECT(stuff_type, stuff_rent_price) ON rent_office TO client;


