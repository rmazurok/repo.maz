����������� ������ �4

����

������� �� ���������������� � ������������ ����� ����������� SQL:
INNER/OUTER JOIN, UNION, WITH, GROUP BY, ORDER BY, HAVING, WHERE.

ճ� ������

1. �������� ���� transactions.txt. ��������� ��� �������, ������.
2. ��������� ������.
 
1) ����� ������� �� ���������� �����/����� �� �������/��������� ����� (WHERE)
2) ��� �����������: ����� �� �볺����, � ���� �� ����� ����������� ����� � ������
3) ��������� ������� ��������� �����������, �� ��� ����� ������ ���� ��� � ���� �볺���� ������,
   �� �������� ������� ���������(��� ����������) (OUTER JOIN)
4) ��������� ������� ��������� ������ ������ �� ��� ����� 
5) ���� ������� ����� ��� ������� ������� (GROUP BY)
6) ���� ������� � ����� ������� ����� > 5 (HAVING)
7) ���� �� ����� ��� ��������� �� �볺��� � ���� ������� (UNION)
8) ���������� ������ ������ ��� ������� �� ����� (ORDER BY)
9) ����� ������ � �������, �� ���� �� 1/2 ������� < 300

1) SELECT resort_id,skipass_id FROM skipasses 
   	WHERE skipass_price < '500' AND skipass_days = '2';

RESULT:
2;8
3;12
3;14

2) SELECT client_name,client_surname FROM clients
	WHERE client_id IN 
	(SELECT client_id FROM rents WHERE rent_status = 'on hand');

RESULT:
"Andiy";"Rezunov"
"Roman";"Mazurok"

3) SELECT managers.manager_name, clients.client_name,clients.client_surname FROM managers LEFT OUTER JOIN clients
	ON clients.manager_id = managers.manager_id;

RESULT:
"Olena";"Roman";"Mazurok"
"Anastasia";"Andiy";"Rezunov"
"Viktoria";"Oleksandr";"Bondar"

4) SELECT COUNT(booking_id) FROM booking
	WHERE booking_begin_date > '2013-04-01'::date AND booking_begin_date < '2013-04-30'::date;

RESULT:
3

5) SELECT resort_id, COUNT(resort_id) FROM tracks
	GROUP BY resort_id;

RESULT:
1;14
3;2
2;5

6) SELECT resort_id, COUNT(resort_id) FROM tracks
	GROUP BY resort_id
	HAVING COUNT(resort_id) > 5;

RESULT:
1;14

7) SELECT client_name, client_surname FROM clients
	UNION SELECT manager_name, manager_surname FROM managers;

RESULT:
"Viktoria";"Yarosh"
"Oleksandr";"Bondar"
"Andiy";"Rezunov"
"Anastasia";"Koretska"
"Olena";"Shostak"
"Roman";"Mazurok"

8) SELECT * FROM skipasses
	ORDER BY skipass_price;	

RESULT:
11;"weekdays";1;200.00;3
7;"weekdays";1;250.00;2
9;"weekend";1;300.00;2
1;"weekdays";1;300.00;1
12;"weekdays";2;350.00;3
13;"weekend";1;350.00;3
5;"weekend";1;350.00;1
14;"weekend";2;450.00;3
8;"weekdays";2;450.00;2
10;"weekend";2;550.00;2
2;"weekdays";2;550.00;1
6;"weekend";2;650.00;1
3;"weekdays";3;800.00;1
4;"weekdays";5;1250.00;1

9) SELECT * FROM rooms
	WHERE room_price < 300 AND room_people_number <= 2 ;

RESULT:
23;"econom";2;250.00;"availible";9

��������: ��� ��������� ������ � ����������� � ��������� ������������� SQL, ������� ������� ������, 
���� ������� ��������� ��������� ������ ���������� SELECT ��� ����� �����������. ���������� �� ��������.
��� ���������� �������� ��������� ���� ��������� ����� ���������� �� ������ �� ������������.