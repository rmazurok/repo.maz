DROP TYPE difficulty CASCADE;
DROP TYPE skipasstype CASCADE;
DROP TYPE roomclass CASCADE;
DROP TYPE roomstatus CASCADE;
DROP TYPE stufftype CASCADE;
DROP TYPE rentstatus CASCADE;
DROP TYPE bail CASCADE;
DROP DOMAIN name_surname CASCADE;
DROP DOMAIN tel_number CASCADE;

CREATE TYPE difficulty AS ENUM ('easy', 'normal', 'hard');
CREATE TYPE skipasstype AS ENUM ('weekdays', 'weekend');
CREATE TYPE roomclass AS ENUM ('econom', 'junior', 'luxe');
CREATE TYPE roomstatus AS ENUM ('availible', 'occupied');
CREATE TYPE stufftype AS ENUM ('snowboard', 'ski', 'helmet', 'glasses', 'armor', 'clothes');
CREATE TYPE rentstatus AS ENUM ('on hand','returned');
CREATE TYPE bail AS ENUM ('passport', 'driving card', 'money');
CREATE DOMAIN name_surname text NOT NULL CHECK (VALUE ~* '^[A-Za-z ]+$');
CREATE DOMAIN tel_number text NOT NULL CHECK (length(VALUE) = 10 AND VALUE ~* '^[0-9]+$');
