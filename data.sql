INSERT INTO managers VALUES
('1','Olena','Shostak','shostak_olena'),
('2','Anastasia','Koretska','koretska_nastia'),
('3','Viktoria','Yarosh','yarosh_vika');

INSERT INTO clients VALUES 
('1','Roman', 'Mazurok','0504384814','4658 7587 4369 6898','1'),
('2','Andiy', 'Rezunov','0664587859','2565 7856 3265 7845','2'),
('3','Oleksandr', 'Bondar','0974568936','4578 1956 4837 8426','3');

INSERT INTO resorts VALUES 
('1','Bukovel','bukovel.com','40','0374567858'),
('2','Dragobrat','gragobrat-go.com','5','0387584687'),
('3','Krasiya','krasiya.info','2','0503106250');

INSERT INTO tracks VALUES 
('1','easy','1000','1'),
('2','easy','1100','1'),
('3','easy','1200','1'),
('4','easy','1250','1'),
('5','easy','15000','1'),
('6','normal','1500','1'),
('7','normal','1700','1'),
('8','normal','2000','1'),
('9','normal','1300','1'),
('10','normal','1250','1'),
('11','hard','1100','1'),
('12','hard','1200','1'),
('13','hard','1700','1'),
('14','hard','1600','1'),
('15','easy','1200','2'),
('16','normal','1500','2'),
('17','normal','1900','2'),
('18','hard','1300','2'),
('19','hard','1400','2'),
('20','easy','1800','3'),
('21','normal','1700','3');

INSERT INTO skipasses VALUES 
('1','weekdays','1','300','1'),
('2','weekdays','2','550','1'),
('3','weekdays','3','800','1'),
('4','weekdays','5','1250','1'),
('5','weekend','1','350','1'),
('6','weekend','2','650','1'),
('7','weekdays','1','250','2'),
('8','weekdays','2','450','2'),
('9','weekend','1','300','2'),
('10','weekend','2','550','2'),
('11','weekdays','1','200','3'),
('12','weekdays','2','350','3'),
('13','weekend','1','350','3'),
('14','weekend','2','450','3');

INSERT INTO hotels VALUES 
('1','Bukovel Hotel','0377842545','Anna','1'),
('2','Vershyna','0374586878','Roman','1'),
('3','Gora','0378569745','Anton','1'),
('4','Koluba','0374785689','Lena','1'),
('5','Dragobrat','0382658975','Anton','2'),
('6','Oselia','0387892563','Viktor','2'),
('7','Dragski','0371257598','Oleksandr','2'),
('8','Karpaty','0372568789','Nadia','2'),
('9','Krasiya','0362547856','Andriy','3'),
('10','Perlyna','0369687542','Anna','3');


INSERT INTO rooms VALUES 
('1','econom','1','300','availible','1'),
('2','econom','2','600','availible','1'),
('3','junior','1','450','availible','1'),
('4','junior','2','700','availible','1'),
('5','luxe','2','1000','availible','1'),
('6','econom','2','300','occupied','2'),
('7','junior','2','700','availible','2'),
('8','luxe','2','1100','availible','2'),
('9','econom','2','400','availible','3'),
('10','junior','2','600','occupied','3'),
('11','luxe','2','800','availible','3'),
('12','econom','2','400','occupied','4'),
('13','junior','2','600','availible','4'),
('14','luxe','2','800','availible','4'),
('15','econom','2','300','availible','5'),
('16','junior','2','500','occupied','5'),
('17','econom','2','400','availible','6'),
('18','junior','2','600','availible','6'),
('19','econom','2','350','occupied','7'),
('20','junior','2','500','availible','7'),
('21','econom','2','400','availible','8'),
('22','junior','2','700','occupied','8'),
('23','econom','2','250','availible','9'),
('24','junior','2','400','availible','9'),
('25','econom','2','300','availible','10'),
('26','junior','2','550','availible','10');

INSERT INTO rent_office VALUES 
('1','snowboard','100'),
('2','ski','90'),
('3','helmet','20'),
('4','glasses','20'),
('5','armor','15'),
('6','clothes','50');

INSERT INTO sells VALUES 
('1','1','2','600','1'),
('2','4','1','1250','2'),
('3','3','2','1600','3'),
('4','2','1','550','1'),
('5','4','2','2500','2');

INSERT INTO booking VALUES 
('1','2013-04-15','2013-04-18','4','2100','2'),
('2','2013-04-25','2013-04-27','9','800','1'),
('3','2013-04-20','2013-04-22','15','600','3');

INSERT INTO rents VALUES 
('1','1','0','1','0','0','0','3','passport','360','0','on hand','2'),
('2','0','2','2','1','0','0','0','money','480','0','on hand','1');